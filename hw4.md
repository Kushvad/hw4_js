# Відповіді на питання:

1. Функція - це підпрограма призначена лля багаторазового виконання конкретної задачі з різними початковими значеннями. Також вони дозволяють структурувати великі програми, зменшувати повторення та ізолювати код.

2. Аргументи треба передавати у функцію, бо вони є значенням для  оголошенних параметрів  при її викликі.

3. Оператор return використовується для передачі значення з тіла функції у зовнішній код. Коли інтерпретатор зустрічає return, він відразу ж виходить з функції  і повертає вказане значення у те місце коду, де була викликана функція.
